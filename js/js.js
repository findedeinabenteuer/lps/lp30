var $loginForm = $("#login-form");
var $recoveryForm = $("#recovery-form");
(function ($) {
  var LoginForm = function (options, form) {
    this.namespace = "login-" + +new Date();
    this.settings = this.prepareSettings(LoginForm.defaults, options);
    this.currentForm = form;
    this.init();
  };
  $.extend($.fn, {
    login: function (options) {
      if (!this.length) {
        return;
      }
      var loginData = $.data(this[0], "loginForm");
      if (loginData) {
        return loginData;
      }
      options = options || {};
      loginData = new LoginForm(options, this[0]);
      $.data(this[0], "loginForm", loginData);
      return loginData;
    },
  });
  $.extend(LoginForm, {
    defaults: {
      url: "/site/checkLogin",
      emailSelector: "#login-email-field",
      passwordSelector: "#login-password-field",
      submitSelector: "#login-submit-btn",
      errorClass: "error-field",
      errorSelector: ".login-form-error",
      fieldSelector: ".login-form-item",
      activeElementEvent: {
        email: ["keyup"],
        password: ["keyup"],
        submit: ["click"],
      },
      allowEvent: ["click", "keyup"],
      onError: function () {},
    },
    prototype: {
      init: function () {
        this.fields = [
          $(this.settings.emailSelector),
          $(this.settings.passwordSelector),
        ];
        this.activeElements = {
          email: $(this.settings.emailSelector),
          password: $(this.settings.passwordSelector),
          submit: $(this.settings.submitSelector),
        };
        this.errorElements = $(this.currentForm).find(
          this.settings.errorSelector
        );
        this.errorList = [];
        this.pendingRequest = 0;
        this.ajaxParams = {
          url: this.settings.url,
          type: "post",
          dataType: "json",
        };
        this.setEvents();
      },
      prepareSettings: function (defaults, settings) {
        var options = $.extend(true, {}, defaults, settings);
        return options;
      },
      setEvents: function () {
        var self = this,
          namespace = this.namespace;
        $.each(this.activeElements, function (index, element) {
          if (!element.length) {
            return;
          }
          var events = {},
            fn = function (e) {
              if (e.type === "keyup") {
                if (e.keyCode === 13) self.submit();
              } else {
                self.submit();
              }
            };
          element.off("." + namespace);
          if (self.settings.activeElementEvent[index]) {
            $.each(self.settings.activeElementEvent[index], function (
              i,
              event
            ) {
              if (!~self.settings.allowEvent.indexOf(event)) {
                return;
              }
              events[event + "." + namespace] = fn;
            });
          }
          element.on(events);
        });
      },
      valid: function () {
        return this.errorList.length === 0;
      },
      validate: function () {
        var self = this,
          isPending = function () {
            return (self.pendingRequest > 0 && self.pendingRequest) || 0;
          },
          working = function () {
            if (isPending()) {
              setTimeout(working, 10);
            } else {
              dfd.resolve(self.valid());
            }
          },
          dfd = $.Deferred();
        var isNewRequest = self.remote();
        if (isNewRequest) {
          setTimeout(working, 10);
        } else {
          working();
        }
        return dfd.promise();
      },
      submit: function () {
        var form = $(this.currentForm);
        $.when(this.validate()).done(function (result) {
          if (result) {
            form.submit();
          }
        });
      },
      startRequest: function () {
        this.pendingRequest++;
      },
      stopRequest: function () {
        this.pendingRequest--;
        if (this.pendingRequest < 0) {
          this.pendingRequest = 0;
        }
      },
      previousValue: function (element) {
        return (
          $.data(element, "previousValue") ||
          $.data(element, "previousValue", { old: null, valid: true })
        );
      },
      remote: function () {
        var self = this,
          previous,
          isNewRequest = false;
        $.each(self.fields, function (i, el) {
          previous = self.previousValue(el);
          if (el.val() !== previous.old) {
            previous.valid = false;
            previous.old = el.val();
            isNewRequest = true;
          }
        });
        if (isNewRequest) {
          this.resetElements();
          this.startRequest();
          $.ajax(
            $.extend(
              true,
              {
                data: $(this.currentForm).serialize(),
                success: function (json) {
                  if (json.status == "error") {
                    var el = $(self.currentForm).find(
                        "[data-error-name=" + json.meta.description.type + "]"
                      ),
                      message = json.meta.description.message;
                    self.errorList.push(message);
                    self.onError(el, message);
                  }
                  if (json.status == "success") {
                    self.errorList = [];
                  }
                },
              },
              self.ajaxParams
            )
          ).done(function () {
            self.stopRequest();
          });
        }
        return isNewRequest;
      },
      onError: function (element, message) {
        element.html(message).fadeIn();
        element
          .closest(this.settings.fieldSelector)
          .addClass(this.settings.errorClass);
        this.settings.onError.apply(this, [element, message]);
      },
      resetElements: function () {
        this.errorElements.hide().html("");
        $(this.settings.fieldSelector).removeClass(this.settings.errorClass);
      },
    },
  });
})(jQuery);
(function ($) {
  var RecoveryForm = function (options, form) {
    this.namespace = "recovery-" + +new Date();
    this.settings = this.prepareSettings(RecoveryForm.defaults, options);
    this.currentForm = form;
    this.init();
  };
  $.extend($.fn, {
    recovery: function (options) {
      if (!this.length) {
        return;
      }
      var recoveryData = $.data(this[0], "recoveryForm");
      if (recoveryData) {
        return recoveryData;
      }
      options = options || {};
      recoveryData = new RecoveryForm(options, this[0]);
      $.data(this[0], "recoveryForm", recoveryData);
      return recoveryData;
    },
  });
  $.extend(RecoveryForm, {
    defaults: {
      url: "/account/remindPassword",
      emailSelector: "#recovery-email-field",
      submitSelector: "#recovery-submit-btn",
      errorClass: "error-field",
      errorSelector: ".recovery-form-error",
      validClass: "valid-field",
      successSelector: ".recovery-form-success",
      fieldSelector: ".recovery-email-item",
      activeElementEvent: { email: ["keyup"], submit: ["click"] },
      allowEvent: ["click", "keyup"],
      onError: function () {},
      onSuccess: function () {},
    },
    prototype: {
      init: function () {
        this.fields = [$(this.settings.emailSelector)];
        this.activeElements = {
          email: $(this.settings.emailSelector),
          submit: $(this.settings.submitSelector),
        };
        this.pendingRequest = 0;
        this.ajaxParams = {
          url: this.settings.url,
          type: "post",
          dataType: "json",
        };
        this.errorElements = $(this.currentForm).find(
          this.settings.errorSelector
        );
        this.successElements = $(this.currentForm).find(
          this.settings.successSelector
        );
        this.setEvents();
      },
      prepareSettings: function (defaults, settings) {
        var options = $.extend(true, {}, defaults, settings);
        return options;
      },
      setEvents: function () {
        var self = this,
          namespace = this.namespace;
        $.each(this.activeElements, function (index, element) {
          if (!element.length) {
            return;
          }
          var events = {},
            fn = function (e) {
              if (e.type === "keyup") {
                if (e.keyCode === 13) self.submit();
              } else {
                self.submit();
              }
            };
          element.off("." + namespace);
          if (self.settings.activeElementEvent[index]) {
            $.each(self.settings.activeElementEvent[index], function (
              i,
              event
            ) {
              if (!~self.settings.allowEvent.indexOf(event)) {
                return;
              }
              events[event + "." + namespace] = fn;
            });
          }
          element.on(events);
        });
      },
      validate: function () {
        var self = this,
          isPending = function () {
            return (self.pendingRequest > 0 && self.pendingRequest) || 0;
          },
          working = function () {
            if (isPending()) {
              setTimeout(working, 10);
            } else {
              dfd.resolve();
            }
          },
          dfd = $.Deferred();
        var isNewRequest = self.remote();
        if (isNewRequest) {
          setTimeout(working, 10);
        } else {
          working();
        }
        return dfd.promise();
      },
      submit: function () {
        $.when(this.validate()).done();
      },
      startRequest: function () {
        this.pendingRequest++;
      },
      stopRequest: function () {
        this.pendingRequest--;
        if (this.pendingRequest < 0) {
          this.pendingRequest = 0;
        }
      },
      previousValue: function (element) {
        return (
          $.data(element, "previousValue") ||
          $.data(element, "previousValue", { old: null, valid: true })
        );
      },
      remote: function () {
        var self = this,
          previous,
          isNewRequest = false,
          redirect = function (url) {
            var exp = new RegExp(/\/\/(www|m)/),
              location = document.location;
            if (!exp.test(url)) {
              url =
                location.protocol +
                "//" +
                location.host +
                (url[0] === "/" ? url : "/" + url);
            }
            window.location.href = url;
          };
        $.each(self.fields, function (i, el) {
          previous = self.previousValue(el);
          if (el.val() !== previous.old) {
            previous.valid = false;
            previous.old = el.val();
            isNewRequest = true;
          }
        });
        if (isNewRequest) {
          this.resetElements();
          this.startRequest();
          $.ajax(
            $.extend(
              true,
              {
                data: $(this.currentForm).serialize(),
                success: function (json) {
                  if (
                    json.status === "error" &&
                    json.meta.code === 302 &&
                    json.meta.redirect
                  ) {
                    redirect(json.meta.redirect);
                    return;
                  }
                  var el = $(self.currentForm).find(
                    "[data-" + json.status + "-name]"
                  );
                  if (json.status === "error") {
                    self.onError(el, json.meta.description.email);
                  }
                  if (json.status === "success") {
                    self.onSuccess(el, json.data.message);
                  }
                },
              },
              self.ajaxParams
            )
          ).done(function () {
            self.stopRequest();
          });
        }
      },
      onError: function (element, message) {
        element.html(message).fadeIn();
        element
          .closest(this.settings.fieldSelector)
          .addClass(this.settings.errorClass)
          .removeClass(this.settings.validClass);
        this.settings.onError.apply(this, [element, message]);
      },
      onSuccess: function (element, message) {
        element.html(message).fadeIn();
        element
          .closest(this.settings.fieldSelector)
          .addClass(this.settings.validClass)
          .removeClass(this.settings.errorClass);
        this.settings.onSuccess.apply(this, [element, message]);
      },
      resetElements: function () {
        this.errorElements.add(this.successElements).hide().html("");
        $(this.settings.fieldSelector).removeClass(this.settings.errorClass);
      },
    },
  });
})(jQuery);
$(function () {
  $loginForm.login();
  $recoveryForm.recovery();
  $(".recovery-password-btn").on("click", function () {
    $(this).closest(".login-form-wrapper").find($loginForm).hide();
    $(this).closest(".login-form-wrapper").find($recoveryForm).fadeIn();
    $("#recovery-email-field")
      .attr("value", $("#login-email-field").val())
      .blur();
    $("#recovery-submit-btn").click();
  });
  $(".login-switch-btn").on("click", function () {
    $(this).closest(".login-form-wrapper").find($recoveryForm).hide();
    $(this).closest(".login-form-wrapper").find($loginForm).fadeIn();
    $("#login-email-field")
      .attr("value", $("#recovery-email-field").val())
      .blur();
    $("#login-submit-btn").click();
  });
});
function addFocusLogin(el) {
  $(el).closest(".login-form-item").addClass("form-item-focus");
}
function removeFocusLogin(el) {
  $(el).closest(".login-form-item").removeClass("form-item-focus");
}
$(".login-form-input input")
  .on("focus", function () {
    addFocusLogin(this);
    $(this).closest(".login-form-item").addClass("form-field-focus");
  })
  .on("blur", function () {
    !this.value ? removeFocusLogin(this) : addFocusLogin(this);
    $(this).closest(".login-form-item").removeClass("form-field-focus");
  })
  .each(function () {
    this.value ? addFocusLogin(this) : removeFocusLogin(this);
  });
function BaseFormFieldManager(fields) {
  if (!fields) {
    return;
  }
  this._fields = fields;
  this.has = function (field) {
    return !!this._fields[field];
  };
  this.setValue = function (field, value) {
    this._fields[field].attr("value", value);
  };
  this.getValue = function (field) {
    return this._fields[field].val();
  };
}
var $baseForm = $("#base-form");
var baseFormManager = new BaseFormFieldManager({
  gender: $baseForm.find('[data-type="gender"]'),
  orientation: $baseForm.find('[data-type="sexual_orientation"]'),
  age: $baseForm.find('[data-type="age"]'),
  location: $baseForm.find('[data-type="location"]'),
  email: $baseForm.find('[data-type="email"]'),
  password: $baseForm.find('[data-type="password"]'),
});
var $mainContainer = $("body");
$("#orientation-field").on("change", function () {
  if (baseFormManager.has("gender") && baseFormManager.has("orientation")) {
    switch ($(this).val()) {
      case "female-male":
        baseFormManager.setValue("gender", "female");
        baseFormManager.setValue("orientation", "hetero");
        $mainContainer.attr("data-orientation-type", "hetero-female");
        break;
      case "male-male":
        baseFormManager.setValue("gender", "male");
        baseFormManager.setValue("orientation", "homo");
        $mainContainer.attr("data-orientation-type", "homo-male");
        break;
      case "female-female":
        baseFormManager.setValue("gender", "female");
        baseFormManager.setValue("orientation", "homo");
        $mainContainer.attr("data-orientation-type", "homo-female");
        break;
      default:
        baseFormManager.setValue("gender", "male");
        baseFormManager.setValue("orientation", "hetero");
        $mainContainer.attr("data-orientation-type", "hetero-male");
        break;
    }
  }
});
var settings = {
  containerSelector: "#suggest",
  useCurrent: true,
  useFormatValue: true,
};
var $locationField = $("#location-field");
var suggest = $locationField.suggestLocation(settings);
$locationField.on("focusout", function (e) {
  if (baseFormManager.has("location")) {
    baseFormManager.setValue("location", $(this).val());
  }
});
(function ($) {
  $.extend($.fn, {
    fDataEmail: function (options) {
      if (!this.length) {
        return;
      }
      var plgFdataEmail = $.data(this[0], "fDataEmail");
      if (plgFdataEmail) {
        return plgFdataEmail;
      }
      options = options || {};
      plgFdataEmail = new fdata(options, this[0]);
      $.data(this[0], "fDataEmail", plgFdataEmail);
      return plgFdataEmail;
    },
  });
  var fdata = function (options) {
    this.namespace = "plgFdataEmail-" + +new Date();
    this.settings = $.extend(true, {}, fdata.defaults, options);
    this.init();
  };
  $.extend(fdata, {
    defaults: { isSkipStep: false, callback: function () {} },
    prototype: {
      init: function () {
        if (!this.settings.hasOwnProperty("emailField")) {
          this.settings.emailField = this.settings.regform.settings.fields.email.element;
        }
        this.fData = this.getUrlVars()["_fData"];
        if (!this.fData) return false;
        this.decodedFData = decodeURIComponent(this.fData);
        this.strFdata = this.b64DecodeUnicode(this.decodedFData);
        try {
          this.fDataObj = JSON.parse(this.strFdata);
          this.email = this.fDataObj.email;
          if (this.isValidEmail(this.email)) {
            this.setEmailValue(this.email);
          }
          if (
            !this.settings.regform != "undefined" &&
            this.settings.isSkipStep
          ) {
            this.settings.regform.settings.nextFunction = this.fnDecoration(
              this.settings.regform.settings.nextFunction,
              this.skipEmailStep
            );
          }
        } catch (error) {}
      },
      setEmailValue: function (emailValue) {
        this.settings.emailField.val(emailValue).blur();
      },
      skipEmailStep: function () {
        var steps = $(this.settings.stepSelector);
        var emailStepIndex = steps.index(
          this.settings.fields.email.element.closest(steps)
        );
        if (emailStepIndex === this.currentStep - 1) {
          this.settings.fields.email.element.focus();
          this.next();
        }
      },
      getUrlVars: function () {
        var vars = {};
        var parts = window.location.href.replace(
          /[?&]+([^=&]+)=([^&]*)/gi,
          function (m, key, value) {
            vars[key] = value;
          }
        );
        return vars;
      },
      isValidEmail: function (email) {
        var pattern = new RegExp(
          /^[a-zA-Z0-9.!#$%&â€™*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
        );
        return this.fDataObj.hasOwnProperty("email") && pattern.test(email);
      },
      b64DecodeUnicode: function (str) {
        return decodeURIComponent(
          atob(str)
            .split("")
            .map(function (c) {
              return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
        );
      },
      fnDecoration: function (fn, decorator) {
        if (fn instanceof Function) {
          return function () {
            decorator.apply(this, arguments);
            fn.apply(this, arguments);
          };
        } else {
          return fn;
        }
      },
    },
  });
})(jQuery);
setTimeout(function () {
  $(document).fDataEmail({ emailField: $("#email-field") });
}, 0);
var $passwordField = document.querySelector("#password-field");
$(".password-icon").on("click", function () {
  $(this).toggleClass("active");
  if ($passwordField.type == "password") {
    $passwordField.setAttribute("type", "text");
  } else {
    $passwordField.setAttribute("type", "password");
  }
});
var options = {
  activeClass: "active",
  nextSelector: ".next-btn",
  prevSelector: ".prev-btn",
  submitSelector: ".submit-btn",
  stepSelector: ".form-step-item",
  fields: {
    age: {
      events: "change",
      selector: "#age-field",
      errorContainer: $("#reg-form #age-field")
        .closest(".form-field-block")
        .find(".form-error-item"),
    },
    location: {
      events: "blur, change",
      selector: "#location-field",
      errorContainer: $("#reg-form #location-field")
        .closest(".form-field-block")
        .find(".form-error-item"),
    },
    email: {
      events: "blur, change",
      selector: "#email-field",
      errorContainer: $("#reg-form #email-field")
        .closest(".form-field-block")
        .find(".form-error-item"),
    },
    password: {
      events: "blur, change",
      selector: "#password-field",
      errorContainer: $("#reg-form #password-field")
        .closest(".form-field-block")
        .find(".form-error-item"),
    },
  },
  validator: {
    errorElement: "div",
    validClass: "valid-field",
    errorFieldClass: "error-field",
    highlight: function (element, errorClass, validClass) {
      var baseForm = $(this.currentForm).baseForm();
      var target = $(baseForm.fields[$(element).data("type")].target);
      $(target)
        .closest(".form-step-item")
        .addClass(this.settings.errorFieldClass)
        .removeClass(this.settings.validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      var baseForm = $(this.currentForm).baseForm();
      var target = $(baseForm.fields[$(element).data("type")].target);
      $(target)
        .closest(".form-step-item")
        .addClass(this.settings.validClass)
        .removeClass(this.settings.errorFieldClass);
    },
  },
  nextFunction: function (oldStep, newStep) {
    $(".pagination-item")
      .removeClass("current")
      .eq(this.currentStep - 1)
      .addClass("current")
      .prevAll()
      .addClass("active");
    $(".step-counter-current").text(this.currentStep);
    $("body").attr("data-current-step", this.currentStep);
  },
  prevFunction: function (oldStep, newStep) {
    $(".pagination-item")
      .removeClass("current active")
      .eq(this.currentStep - 1)
      .addClass("current")
      .prevAll()
      .addClass("active");
    $(".step-counter-current").text(this.currentStep);
    $("body").attr("data-current-step", this.currentStep);
  },
};
var regform = $("#reg-form").stepSwitcher(options);
$("#reg-form").userRecovery({ errorSelector: '[id="UserForm[email]-error"]' });
$("body").attr("data-current-step", 1);
$(".step-counter-current").html(regform.currentStep);
$(".step-counter-total").html(regform.steps.length);
$(document).ready(function () {
  var fieldSettings = {
    fieldContainer: ".form-item",
    activeClass: "is-active",
    focusClass: "is-focused",
    select: {
      container: ".form-select",
      selectedValue: ".select-value",
      dropdown: ".select-dropdown",
      dropdownItem: "select-item",
    },
  };
  function addFocus(el) {
    $(el)
      .closest(fieldSettings.fieldContainer)
      .addClass(fieldSettings.focusClass);
  }
  function removeFocus(el) {
    $(el)
      .closest(fieldSettings.fieldContainer)
      .removeClass(fieldSettings.focusClass);
  }
  var selectClone;
  $(fieldSettings.select.container)
    .find("select")
    .each(function () {
      $(this)
        .find("option")
        .each(function () {
          if ($(this).is("[selected]")) {
            $(this)
              .closest(fieldSettings.select.container)
              .find(fieldSettings.select.selectedValue)
              .html($(this).html());
            addFocus(this);
          }
          selectClone +=
            '<div class="' +
            fieldSettings.select.dropdownItem +
            '" value="' +
            $(this).val() +
            '">' +
            $(this).html() +
            "</div>";
        })
        .closest(fieldSettings.select.container)
        .find(fieldSettings.select.dropdown)
        .append($(selectClone));
      selectClone = "";
    });
  $(document).on("click", fieldSettings.select.container, function (e) {
    var $this = $(this);
    var $selectItem = $(e.target).closest(
      "." + fieldSettings.select.dropdownItem
    );
    if ($selectItem.length) {
      $this.removeClass(fieldSettings.activeClass);
      addFocus(this);
      $this.find(fieldSettings.select.selectedValue).html($selectItem.html());
      $this.find("select").val($selectItem.attr("value")).change();
      return;
    }
    $this.toggleClass(fieldSettings.activeClass);
  });
  $(document).on("click", function (e) {
    var $target = $(e.target),
      $select = $(fieldSettings.select.container);
    if (!$target.closest(fieldSettings.select.container).length) {
      $select.removeClass(fieldSettings.activeClass);
    } else {
      $select
        .not($target.closest(fieldSettings.select.container))
        .removeClass(fieldSettings.activeClass);
    }
  });
  $(".form-input input")
    .on("focus", function () {
      addFocus(this);
      $(this)
        .closest(fieldSettings.fieldContainer)
        .addClass(fieldSettings.activeClass);
    })
    .on("blur", function () {
      !this.value ? removeFocus(this) : addFocus(this);
      $(this)
        .closest(fieldSettings.fieldContainer)
        .removeClass(fieldSettings.activeClass);
    })
    .each(function () {
      this.value ? addFocus(this) : removeFocus(this);
    });
  $("#orientation-item .form-label").html("Ich bin:");
  $("#age-item .form-label").html("Mein Alter:");
  $("#location-item .form-label").html("Mein Ort:");
  $("#email-item .form-label").html("Meine E-Mail-Adresse:");
  $("#password-item .form-label").html("Mein Passwort:");
});
$(".form-input input, .login-form-field input").removeAttr("placeholder");
$(function () {
  var $regformBlock = $(".reg-form-block");
  var $loginformBlock = $(".login-form-block");
  var switchBtnClass = ".switch-form-btn";
  var $overlay = $(".overlay");
  $(switchBtnClass).on("click", function () {
    var formName = $(this).attr("data-form-name");
    $(this)
      .removeClass("is-active")
      .siblings(switchBtnClass)
      .addClass("is-active");
    switch (formName) {
      case "login":
        $overlay.fadeIn();
        $regformBlock.hide();
        $loginformBlock.stop(1, 1).fadeIn();
        break;
      case "register":
        $overlay.fadeOut();
        $loginformBlock.hide();
        $regformBlock.stop(1, 1).fadeIn();
        break;
    }
  });
  $overlay.on("click", function () {
    $(this).fadeOut();
    $loginformBlock.hide();
    $regformBlock.fadeIn();
    $('[data-form-name="register"]')
      .removeClass("is-active")
      .siblings(switchBtnClass)
      .addClass("is-active");
  });
  $(".members-item").on("click", function () {
    var $nextStep = $(".next-btn");
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $nextStep.addClass("hvr-wobble-horizontal");
    setTimeout(function () {
      $nextStep.removeClass("hvr-wobble-horizontal");
      if ($("#age-item").hasClass("valid-field")) {
        return;
      } else {
        $("#age-item .form-select").addClass("is-active");
      }
    }, 1000);
  });
});
