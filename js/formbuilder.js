$(function () {
  var $regForm = $(".js-register-form");

  $(".js-open-register-form").on("click", function () {
    $regForm.removeClass("form-hidden hidden");
    if ($regForm.hasClass("js-overlay")) {
      $(document.body).find(".modal-overlay").remove();
      $(document.body).append('<div class="modal-overlay"></div>');
    }
  });
  $(".js-close-register-form").on("click", function () {
    $regForm.addClass("form-hidden");
    if ($regForm.hasClass("js-overlay")) {
      $(document.body).find(".modal-overlay").remove();
    }
  });

  $("input.js-visual-checked").on("keyup blur", function (e) {
    $(this).parent().addClass("visual-checked");

    var rules = $(this).attr("data-validator").split("|"),
      value = $(this).val(),
      validator = new Validator(),
      ruleError = "",
      error = false;

    for (var i = 0; i < rules.length; i++) {
      var rule = rules[i],
        compare = $(this).attr("data-" + rule + "-compare");

      if (typeof compare != "undefined") {
        if (!validator.methods[rule](value, compare)) {
          error = true;
          ruleError = rule;
          break;
        }
      } else {
        if (!validator.methods[rule](value)) {
          error = true;
          ruleError = rule;
          break;
        }
      }
    }

    if (error == false) {
      $(this).parent().addClass("visual-checked-success");

      if (e.type == "blur") $(this).siblings("span.required").remove();
    } else {
      $(this).parent().removeClass("visual-checked-success");

      if (e.type == "blur") {
        var message = $(this).data(ruleError.toLowerCase() + "-message");
        if (message) {
          $(this).siblings("span.required").remove();
          $(this).after('<span class="required">' + message + "</span>");
        }
      }
    }
  });
});
